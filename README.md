# Front-end development and UI
Front-end development is the technical implementation of the software’s User Interface (UI)

[link](https://www.toptal.com/ui-ux-frontend/job-description)

## Major Front-end development
[HTML5_5장](./sources/05/radio-check.html)

[HTML5 + CSS3_11장](./sources/11/product-result.html)

[HTML5 + CSS3 + jQuery](./sources/jQuery/01 jQuery 라이브러리 응용/실습_문제/JQ_5.html)

## Extension
[PYTHON-APP](https://github.com/devsacti/PYTHON-APP/tree/main/Impls/primary_pycharm_prj)


## Reference
Do it! HTML+CSS+자바스크립트 웹 표준의 정석